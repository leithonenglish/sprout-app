import axios from 'axios';
import store from '@/stores/store';

const instance = axios.create({
    baseURL: process.env.VUE_APP_BASE_URL,
    headers: {
        Authorization: `Bearer ${ store.getters['auth/token'] }`,
    },
});

export default instance;
