import Vue from 'vue';
import App from './app.vue';
import router from './router';
import store from './stores/store';
import PrettyCheckbox from 'pretty-checkbox-vue';
import VeeValidate from 'vee-validate';
import VueMoment from 'vue-moment';
import vuescroll from 'vuescroll/dist/vuescroll-native';
import 'vuescroll/dist/vuescroll.css';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import BootstrapVue from 'bootstrap-vue';

Vue.config.productionTip = false;
Vue.use(PrettyCheckbox);
Vue.use(VeeValidate);
Vue.use(VueMoment);
Vue.use(vuescroll);
Vue.use(BootstrapVue);

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
