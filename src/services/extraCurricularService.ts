import axios from '@/configs/axios-post-auth';

export default {
    fetchAll() {
        return new Promise((resolve, reject) => {
            axios.get('/extraCurricular/getAll')
            .then((res) => {
                resolve(res.data);
            })
            .catch((err) => {
                reject(err.response);
            });
        });
    },
};
