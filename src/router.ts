import Vue from 'vue';
import Router from 'vue-router';
import store from '@/stores/store';

Vue.use(Router);

const ifNotAuthenticated = (to, from, next) => {
  if (!store.getters['auth/isAuthenticated']) {
    next();
    return;
  }
  next('/');
};

const ifAuthenticated = (to, from, next) => {
  if (store.getters['auth/isAuthenticated']) {
    next();
    return;
  }
  next('/auth/login');
};

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      component: () => import('./views/main.vue'),
      children: [
        {
          path: '',
          name: 'home',
          component: () => import('./views/dashboard/index.vue'),
          beforeEnter: ifAuthenticated,
        },
        {
          path: '/students',
          name: 'students',
          component: () => import('./views/students.vue'),
        },
        {
          path: '/applications',
          name: 'applications',
          component: () => import('./views/applications.vue'),
        },
        {
          path: '/guardians',
          name: 'guardians',
          component: () => import('./views/guardians.vue'),
        },
        {
          path: '/curriculars',
          name: 'curriculars',
          component: () => import('./views/extraCurriculars/index.vue'),
        },
        {
          path: '/classes',
          name: 'classes',
          component: () => import('./views/classes.vue'),
        },
      ],
    },
    {
      path: '/auth',
      component: () => import('./views/auth/index.vue'),
      children: [
        {
          path: 'login',
          name: 'login',
          component: () => import('./views/auth/login.vue'),
          beforeEnter: ifNotAuthenticated,
        },
      ],
    },
  ],
});
