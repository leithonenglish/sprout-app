import TimePeriod from '@/models/TimePeriod';

export default class DayWithTime {
    public day: string;
    public timePeriod: TimePeriod;
}
