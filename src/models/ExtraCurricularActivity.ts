import DayWithTime from '@/models/DayWithTime';

export default class ExtraCurricularActivity {
    public id: string;
    public name: string;
    public description: string;
    public activeDays: DayWithTime[];
    public enrollees: string[];
}
