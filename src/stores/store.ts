import Vue from 'vue';
import Vuex from 'vuex';
import auth from './modules/auth';
import adderButton from './modules/adderButton';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    page: '',
  },
  getters: {
    page: (ztate) => ztate.page,
  },
  mutations: {
    setPage(ztate, page) {
      ztate.page = page;
    },
  },
  actions: {
  },
  modules: {
    auth,
    adderButton,
  },
});
