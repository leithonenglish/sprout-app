const state = {
    visible: false,
    hasAccess: true,
    callback: () => null,
};

const getters = {
    isVisible: (ztate) => ztate.visible,
    hasAccess: (ztate) => ztate.hasAccess,
    callback: (ztate) => ztate.callback(),
};

const actions = {
    // tslint:disable-next-line:no-shadowed-variable
    triggerCallback({ state }) {
        state.callback();
    },
};

const mutations = {
    setCallback(ztate, method) {
        ztate.callback = method;
    },
    setVisibility(ztate, visible) {
        ztate.visible = visible;
    },
};

export default {
    namespaced: true,
    state,
    mutations,
    actions,
    getters,
};
