import axios from '../../configs/axios-auth';

const state = {
    token: localStorage.getItem('token'),
    firstName: localStorage.getItem('firstName'),
    lastName: localStorage.getItem('lastName'),
};

const getters = {
    token: (ztate) => ztate.token,
    isAuthenticated: (ztate) => !!ztate.token,
    firstName: (ztate) => ztate.firstName,
    lastName: (ztate) => ztate.lastName,
};

const actions = {
    authenticate: ({ commit }, params) => {
        return new Promise((resolve, reject) => {
            axios.post('/authenticate', params)
            .then((res) => {
                const data = res.data;
                const profile = data.user.profile;
                const authParams = { token: data.token, firstName: profile.firstName, lastName: profile.lastName };
                localStorage.setItem('token', authParams.token);
                localStorage.setItem('firstName', authParams.firstName);
                localStorage.setItem('lastName', authParams.lastName);
                commit('setAuthData', authParams);
                resolve();
            })
            .catch((err) => {
                localStorage.removeItem('token');
                commit('removeAuthData');
                reject(err.response);
            });
        });
    },
    logout: ({ commit }) => {
        return new Promise((resolve) => {
            localStorage.clear();
            commit('removeAuthData');
            resolve();
        });
    },
};

const mutations = {
    setAuthData(ztate, data) {
        ztate.token = data.token;
        ztate.firstName = data.firstName;
        ztate.lastName = data.lastName;
    },
    removeAuthData(ztate) {
        ztate.token = null;
        ztate.user = null;
    },
};

export default {
    namespaced: true,
    state,
    mutations,
    actions,
    getters,
};
